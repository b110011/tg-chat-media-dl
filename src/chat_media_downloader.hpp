#ifndef __SRC_CHATMEDIADOWNLOADER_HPP__
#define __SRC_CHATMEDIADOWNLOADER_HPP__

#include "chat_iterator.hpp"

class ChatMediaDownloader final
    : public ChatIterator
{
public:

    ChatMediaDownloader(
        Client &client,
        td_api::int53 self_id,
        td_api::int53 chat_id,
        td_api::int53 stop_id,
        ChatParticipants participants
    );

    ~ChatMediaDownloader() override;

private:

    void restoreFilesInfo();

    void saveFilesInfo();


    void handle(TgMessagePtr message, td::td_api::int53 sender_id) final;

    void downloadFile(td_api::int32 file_id);

private:

    struct FileInfo
    {
        td_api::int53 const msg_id_;
        td_api::int53 const sender_id_;
        td_api::int32 const date_;
        std::optional<td_api::int53> file_id_;
        bool const is_in_album_;
        td_api::string path_;
    };

    std::map<td_api::int32 /* file id */, FileInfo> files_info_;
    std::unordered_map<
        td_api::int53,  // msg id
        td_api::int32  // file id
    > processed_messages_;
    std::size_t const cur_path_length_;
};

#endif // __SRC_CHATMEDIADOWNLOADER_HPP__
