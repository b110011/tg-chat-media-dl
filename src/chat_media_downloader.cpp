#include "chat_media_downloader.hpp"

#include "client.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <limits>
#include <locale>
#include <vector>

namespace {

char const *k_cache_file_location_ = "./downloads/cache.csv";

class CsvWhitespace final
    : public std::ctype<char>
{
public:

    CsvWhitespace(std::size_t refs = 0)
        : ctype(make_table(), false, refs)
    {}

    static mask const *make_table()
    {
        static std::vector<mask> v(classic_table(), classic_table() + table_size);
        v[','] |=  space;
        return v.data();
    }

};

constexpr td_api::int53
generateNewFileId(td_api::int53 msg_id, td_api::int32 file_id) noexcept
{
    return msg_id * 1000 + file_id;
}

} // namespace

ChatMediaDownloader::ChatMediaDownloader(
    Client &client,
    td_api::int53 self_id,
    td_api::int53 chat_id,
    td_api::int53 stop_id,
    ChatParticipants participants
)
    : ChatIterator(client, self_id, chat_id, stop_id, participants)
    , cur_path_length_{std::filesystem::current_path().string().size() + 1}
{
    client_.addHandler(
        [this](td_api::object_ptr<td_api::updateFile> update)
        {
            auto const &local_file = update->file_->local_;
            if (local_file->is_downloading_completed_)
            {
                files_info_.at(update->file_->id_).path_ = local_file->path_.substr(cur_path_length_);
            }
        }
    );

    restoreFilesInfo();
}

ChatMediaDownloader::~ChatMediaDownloader()
{
    client_.removeHandler<td_api::updateFile>();
    saveFilesInfo();
}

void
ChatMediaDownloader::restoreFilesInfo()
{
    if (!std::filesystem::exists(k_cache_file_location_))
    {
        return;
    }

    std::ifstream ifs{k_cache_file_location_};
    ifs.imbue(std::locale(ifs.getloc(), new CsvWhitespace));

    ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    td_api::int53 msg_id = 0;
    td_api::int53 sender_id = 0;
    td_api::int32 file_id = 0;
    td_api::int32 date = 0;
    td_api::string path;
    bool is_in_album = false;

    while(ifs >> msg_id >> sender_id >> file_id >> date >> path >> is_in_album)
    {
        auto file_info_it_ = files_info_.find(file_id);
        FileInfo file_info{msg_id, sender_id, date, std::nullopt, is_in_album, path};

        if (file_info_it_ == files_info_.end())
        {
            files_info_.emplace_hint(file_info_it_, file_id, std::move(file_info));
        }
        else
        {
            file_info.file_id_ = file_id;
            files_info_.emplace(generateNewFileId(msg_id, file_id), std::move(file_info));
        }

        processed_messages_.emplace(msg_id, file_id);
    }
}

void
ChatMediaDownloader::saveFilesInfo()
{
    std::ofstream ofs{k_cache_file_location_};
    ofs << "MSG_ID,SENDER_ID,FILE_ID,DATE,PATH,IS_ALBUM\n";

    for (auto const &[file_id, file_info] : files_info_)
    {
        ofs << file_info.msg_id_ << ','
            << file_info.sender_id_ << ','
            << file_info.file_id_.value_or(file_id) << ','
            << file_info.date_ << ','
            << file_info.path_ << ','
            << file_info.is_in_album_ << '\n';
    }
}

namespace {

td_api::object_ptr<td_api::file>
extractFileId(td_api::messagePhoto &content)
{
    auto &photoSizes = content.photo_->sizes_;

    using PhotoSizePtr = td_api::object_ptr<td_api::photoSize>;
    std::sort(
        photoSizes.begin(),
        photoSizes.end(),
        [] (PhotoSizePtr const &lhs, PhotoSizePtr const &_rhs)
        {
            if (lhs->width_ == _rhs->width_)
            {
                return lhs->height_ < _rhs->height_;
            }

            return lhs->width_ < _rhs->width_;
        }
    );

    return std::move(photoSizes.back()->photo_);
}

td_api::object_ptr<td_api::file>
extractFileId(td_api::messageVideo &content)
{
    return std::move(content.video_->video_);
}

} // namespace

void
ChatMediaDownloader::handle(TgMessagePtr message, td::td_api::int53 sender_id)
{
    td_api::object_ptr<td_api::file> file;
    td_api::downcast_call(
        *message->content_,
        overloaded(
            [&file](td_api::messagePhoto &content)
            {
                std::cout << "\tDownloading photo" << std::endl;
                file = extractFileId(content);
            },
            [&file](td_api::messageVideo &content)
            {
                std::cout << "\tDownloading video" << std::endl;
                file = extractFileId(content);
            },
            [](auto &update)
            {
                std::cout << "\tSkipping message because it has no media" << std::endl;
            }
        )
    );

    if (file == nullptr)
    {
        return;
    }

    auto msg_date = message->date_;
    auto &forward_info = message->forward_info_;
    if (forward_info)
    {
        // We always want to preserve original date even if original message is deleted.
        msg_date = forward_info->date_;
    }

    // User could terminate app before downloader properly saved information.
    auto const &local_file = file->local_;
    if (local_file->is_downloading_completed_)
    {
        std::cout << "\tFile has been already downloaded!" << std::endl;

        FileInfo file_info{
            message->id_,
            sender_id,
            msg_date,
            std::nullopt,
            static_cast<bool>(message->media_album_id_),
            local_file->path_.substr(cur_path_length_)
        };

        auto file_info_it_ = files_info_.find(file->id_);
        if (file_info_it_ == files_info_.end())
        {
            std::cout << "\tNo info in local cache file! Adding..." << std::endl;
            files_info_.emplace_hint(file_info_it_, file->id_, std::move(file_info));

            return;
        }
        else if (file_info_it_->second.path_.empty())
        {
            std::cout << "\tRestoring missing path to downloaded file..." << std::endl;
            file_info_it_->second.path_ = file_info.path_;
        }

        if (file_info_it_->second.msg_id_ != file_info.msg_id_)
        {
            auto const newFileId = generateNewFileId(file_info.msg_id_, file->id_);

            file_info_it_ = files_info_.find(newFileId);
            if(file_info_it_ == files_info_.end())
            {
                std::cout << "\tNo info in local cache file! Adding..." << std::endl;

                file_info.file_id_ = file->id_;
                files_info_.emplace(newFileId, std::move(file_info));
            }
            else if (file_info_it_->second.path_.empty())
            {
                std::cout << "\tRestoring missing path to downloaded file..." << std::endl;
                file_info_it_->second.path_ = file_info.path_;
            }

            std::cout << "\tMessage contains duplicated media (" << file->id_ << ")" << std::endl;
        }

        return;
    }

    // We could download file but telegram changed file id. Why? Who knows...
    auto processed_msg_it_ = processed_messages_.find(message->id_);
    if (processed_msg_it_ != processed_messages_.end())
    {
        auto file_info_it_ = files_info_.find(processed_msg_it_->second);
        if (std::filesystem::exists(file_info_it_->second.path_))
        {
            if (file_info_it_->first != file->id_)
            {
                std::cout << "\tFile id has changed from " << file_info_it_->first << " to " << file->id_ << "!" << std::endl;

                files_info_.emplace(file->id_, std::move(file_info_it_->second));
                files_info_.erase(file_info_it_);

                // We won't use this but let's update anyway.
                processed_msg_it_->second = file->id_;
            }

            std::cout << "\tFile has been already downloaded!" << std::endl;
            return;
        }

        // We won't use these but let's remove anyway.
        processed_messages_.erase(processed_msg_it_);
        files_info_.erase(file_info_it_);
    }

    files_info_.try_emplace(
        file->id_,
        FileInfo{
            message->id_,
            sender_id,
            msg_date,
            std::nullopt,
            static_cast<bool>(message->media_album_id_),
            ""
        }
    );

    downloadFile(file->id_);
}

void
ChatMediaDownloader::downloadFile(td_api::int32 fileId)
{
    // https://github.com/tdlib/td/issues/740
    // https://github.com/tdlib/td/issues/820
    client_.send(
        td_api::make_object<td_api::downloadFile>(
            fileId,
            /* priority = */ 32,
            /* offset = */ 0,
            /* limit = */ 0,
            /* synchronous = */ true
        )
    );
    client_.pullUpdates(1);
}
