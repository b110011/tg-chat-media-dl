#ifndef __SRC_TYPES_HPP__
#define __SRC_TYPES_HPP__

#include <td/telegram/td_api.hpp>

#include <functional>

namespace td_api = td::td_api;

using TgObjectPtr = td_api::object_ptr<td_api::Object>;
using TgRequestPtr = td_api::object_ptr<td_api::Function>;

using ResponseHandler = std::function<void(TgObjectPtr)>;

#endif // __SRC_TYPES_HPP__
