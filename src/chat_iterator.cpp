#include "chat_iterator.hpp"

#include "client.hpp"

#include <iostream>

ChatIterator::ChatIterator(
    Client &client,
    td_api::int53 self_id,
    td_api::int53 chat_id,
    td_api::int53 stop_id,
    ChatParticipants participants
)
    : client_{client}
    , chat_id_{chat_id}
    , self_id_{self_id}
    , stop_id_{stop_id}
    , participants_{participants}
    , is_running_{false}
{}

void
ChatIterator::run()
{
    is_running_ = true;

    iterate(0);

    while (is_running_)
    {
        client_.pullUpdates(1);
    }
}

void
ChatIterator::iterate(td_api::int53 from_msg_id)
{
    client_.send(
        td_api::make_object<td_api::getChatHistory>(
            chat_id_,
            from_msg_id,
            /* offset = */ 0,
            /* limit = */ 1,
            /* only_local = */ false
        ),
        [this](td_api::object_ptr<td_api::messages> messages)
        {
            if (auto const last_id = handle(std::move(messages)))
            {
                iterate(*last_id);
                return;
            }

            std::cout << "Download is finished!" << std::endl;
            is_running_ = false;
        }
    );
}

std::optional<td_api::int53>
ChatIterator::handle(td_api::object_ptr<td_api::messages> messages)
{
    std::optional<td_api::int53> last_id;

    for (auto && message : messages->messages_)
    {
        if (!message)
        {
            continue;
        }

        last_id = message->id_;

        if (last_id == stop_id_)
        {
            std::cout << "Reached " << *last_id << " message id!" << std::endl;
            return std::nullopt;
        }

        std::cout << "Processing message with " << message->id_ << " id" << std::endl;

        auto optSenderId = tryGetSenderId(*message);
        if (!optSenderId)
        {
            std::cout << "\tSkipping message because it's forwarded" << std::endl;
            continue;
        }

        handle(std::move(message), *optSenderId);
    }

    return last_id;
}

std::optional<td::td_api::int53>
ChatIterator::tryGetSenderId(td_api::message &message) const
{
    auto &forward_info_ = message.forward_info_;
    if (forward_info_)
    {
        if (forward_info_->origin_->get_id() == td_api::messageOriginUser::ID)
        {
            auto const &origin = td::move_tl_object_as<td_api::messageOriginUser>(forward_info_->origin_);
            if (isAcceptableSenderId(origin->sender_user_id_))
            {
                return origin->sender_user_id_;
            }
        }

        return {};
    }

    std::optional<td::td_api::int53> sender_id;
    td_api::downcast_call(
        *message.sender_id_,
        overloaded(
            [&sender_id](td_api::messageSenderChat &chat)
            {
                sender_id = chat.chat_id_;
            },
            [&sender_id](td_api::messageSenderUser &user)
            {
                sender_id = user.user_id_;
            }
        )
    );

    if (sender_id && isAcceptableSenderId(*sender_id))
    {
        return *sender_id;
    }

    return {};
}

bool
ChatIterator::isAcceptableSenderId(td::td_api::int53 sender_id) const noexcept
{
    switch (participants_)
    {
        case ChatParticipants::Participant:
            return sender_id == chat_id_;

        case ChatParticipants::Self:
            return sender_id == self_id_;

        default:
            return sender_id == chat_id_ || sender_id == self_id_;
    }
}
