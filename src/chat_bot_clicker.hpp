#ifndef __SRC_CHATBOTCLICKER_HPP__
#define __SRC_CHATBOTCLICKER_HPP__

#include "chat_iterator.hpp"

class ChatBotClicker final
    : public ChatIterator
{
public:

    ChatBotClicker(
        Client &client,
        td_api::int53 self_id,
        td_api::int53 chat_id,
        td_api::int53 stop_id,
        std::size_t row_idx,
        std::size_t col_idx
    );

private:

    void handle(TgMessagePtr message, td::td_api::int53 sender_id) final;

    void handle(td_api::int53 msg_id, td_api::inlineKeyboardButtonTypeCallback &btn);

private:

    std::size_t const row_idx_;
    std::size_t const col_idx_;
};

#endif // __SRC_CHATBOTCLICKER_HPP__
