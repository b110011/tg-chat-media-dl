#include "client.hpp"

Client::Client()
    : client_manager_{std::make_unique<td::ClientManager>()}
    , client_id{client_manager_->create_client_id()}
    , cur_query_id{0}
{
    send(td_api::make_object<td_api::getOption>("version"));
}

Client::~Client()
{
    pullUpdates(1);
}

void
Client::send(TgRequestPtr request, ResponseHandler handler)
{
    auto const query_id = cur_query_id++;

    if (handler)
    {
        handlers_.emplace(query_id, std::move(handler));
    }

    client_manager_->send(client_id, query_id, std::move(request));
}

std::pair<TgObjectPtr, bool>
Client::receive(double timeout)
{
    auto response = client_manager_->receive(timeout);
    if (!response.object)
    {
        return {nullptr, false};
    }

    // m_debug/std::cout << response.request_id << " " << to_string(response.object) << std::endl;

    if (auto it = handlers_.find(response.request_id); it != handlers_.end())
    {
        it->second(std::move(response.object));
        handlers_.erase(it);

        return {nullptr, true};
    }

    if (auto it = handlers_by_type_.find(response.object->get_id()); it != handlers_by_type_.end())
    {
        it->second(std::move(response.object));
        return {nullptr, true};
    }

    return {std::move(response.object), true};
}

void
Client::pullUpdates(double timeout)
{
    while (receive(timeout).second);
}

void
Client::removeHandler(std::int32_t id)
{
    handlers_by_type_.erase(id);
}
