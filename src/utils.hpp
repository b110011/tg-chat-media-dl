#ifndef __SRC_UTILS_HPP__
#define __SRC_UTILS_HPP__

// overloaded

namespace detail {

template <class... Fs>
struct overload;

template <class F>
struct overload<F> : public F
{
    explicit overload(F f) : F(f) {}
};

template <class F, class... Fs>
struct overload<F, Fs...>
    : public overload<F>
    , public overload<Fs...>
{
  overload(F f, Fs... fs) : overload<F>(f), overload<Fs...>(fs...) {}

  using overload<F>::operator();
  using overload<Fs...>::operator();
};

}  // namespace detail

template <class... F>
auto overloaded(F... f)
{
    return detail::overload<F...>(f...);
}

// https://stackoverflow.com/questions/6512019/can-we-get-the-type-of-a-lambda-argument

template<typename Ret, typename Arg, typename... Rest>
Arg first_argument_helper(Ret(*) (Arg, Rest...));

template<typename Ret, typename F, typename Arg, typename... Rest>
Arg first_argument_helper(Ret(F::*) (Arg, Rest...));

template<typename Ret, typename F, typename Arg, typename... Rest>
Arg first_argument_helper(Ret(F::*) (Arg, Rest...) const);

template <typename F>
decltype(first_argument_helper(&F::operator())) first_argument_helper(F);

template <typename T>
using first_argument = decltype(first_argument_helper(std::declval<T>()));

template<typename _T, typename _U>
_T const &
to(td_api::object_ptr<_U> const &p)
{
    return  static_cast<_T const &>(*p);
}

#endif // __SRC_UTILS_HPP__
