#ifndef __SRC_AUTH_HPP__
#define __SRC_AUTH_HPP__

#include "types.hpp"

class Client;

class Authorization final
{
public:

    Authorization(Client &client) noexcept;

    bool handle(TgObjectPtr object);

private:

    void send(TgRequestPtr request, ResponseHandler handler = {});

private:

    Client &client_;
    std::uint64_t query_id_;
    td_api::object_ptr<td_api::updateAuthorizationState> cur_state_;
};

#endif // __SRC_AUTH_HPP__
