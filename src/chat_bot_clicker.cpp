#include "chat_bot_clicker.hpp"

#include "client.hpp"

#include <iostream>

ChatBotClicker::ChatBotClicker(
    Client &client,
    td_api::int53 self_id,
    td_api::int53 chat_id,
    td_api::int53 stop_id,
    std::size_t row_idx,
    std::size_t col_idx
)
    : ChatIterator(client, self_id, chat_id, stop_id, ChatParticipants::Participant)
    , row_idx_{row_idx}
    , col_idx_{col_idx}
{}

void
ChatBotClicker::handle(TgMessagePtr message, td::td_api::int53 /*sender_id*/)
{
    if (!message->reply_markup_)
    {
        std::cout << "\tNo buttons!" << std::endl;
        return;
    }

    td_api::downcast_call(
        *message->reply_markup_,
        overloaded(
            [this, msg_id = message->id_](td_api::replyMarkupInlineKeyboard &keyboard)
            {
                td_api::downcast_call(
                    *keyboard.rows_[row_idx_][col_idx_]->type_,
                    overloaded(
                        [this, msg_id](td_api::inlineKeyboardButtonTypeCallback &btn)
                        {
                            handle(msg_id, btn);
                        },
                        [this, msg_id](td_api::inlineKeyboardButtonTypeUrl &btn)
                        {
                            std::cout << "\t[url]: " << btn.url_ << std::endl;
                        },
                        [](auto &update) {}
                    )
                );
            },
            [](auto &update) {}
        )
    );
}

void
ChatBotClicker::handle(td_api::int53 msg_id, td_api::inlineKeyboardButtonTypeCallback &btn)
{
    client_.send(
        td_api::make_object<td_api::getCallbackQueryAnswer>(
            chat_id_,
            msg_id,
            td_api::make_object<td_api::callbackQueryPayloadData>(btn.data_)
        ),
        [](td_api::object_ptr<td_api::callbackQueryAnswer> answer)
        {
            if (!answer->text_.empty())
            {
                std::cout << "\t[text]: " << answer->text_ << std::endl;
            }

            if (!answer->url_.empty())
            {
                std::cout << "\t[url]: " << answer->url_ << std::endl;
            }
        }
    );
    client_.pullUpdates(1);
}
