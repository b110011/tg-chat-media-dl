#ifndef __SRC_CHATITERATOR_HPP__
#define __SRC_CHATITERATOR_HPP__

#include "types.hpp"

#include <optional>
#include <map>
#include <unordered_map>

class Client;

enum ChatParticipants: char
{
    Participant = 'p',
    Self = 's',
    Both = 'b',
};

class ChatIterator
{
public:

    ChatIterator(
        Client &client,
        td_api::int53 selfId,
        td_api::int53 chatId,
        td_api::int53 stopId,
        ChatParticipants participants
    );

    virtual ~ChatIterator() = default;


    void run();

protected:

    using TgMessagePtr = td_api::object_ptr<td_api::message>;

private:

    void iterate(td_api::int53 from_msg_id);


    std::optional<td_api::int53> handle(td_api::object_ptr<td_api::messages> messages);

    virtual void handle(TgMessagePtr message, td::td_api::int53 sender_id) = 0;


    std::optional<td::td_api::int53> tryGetSenderId(td_api::message &message) const;

    bool isAcceptableSenderId(td::td_api::int53 sender_id) const noexcept;

protected:

    Client &client_;
    td_api::int53 const chat_id_;

private:

    td_api::int53 const self_id_;
    td_api::int53 const stop_id_;
    ChatParticipants const participants_;
    bool is_running_;
};

#endif // __SRC_CHATITERATOR_HPP__
