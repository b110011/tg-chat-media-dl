#include "auth.hpp"

#include "client.hpp"

#include <algorithm>
#include <iostream>

Authorization::Authorization(Client &client) noexcept
    : client_{client}
    , query_id_{0}
{}

bool
Authorization::handle(TgObjectPtr object)
{
    if (object->get_id() != td_api::updateAuthorizationState::ID)
    {
        // There will be a lot of 'updateOption'.
        return false;
    }

    query_id_++;

    cur_state_ = td::move_tl_object_as<td_api::updateAuthorizationState>(object);
    switch (cur_state_->authorization_state_->get_id())
    {
        case td_api::authorizationStateReady::ID:
            std::cout << "Authorization is completed" << std::endl;
            return true;

        case td_api::authorizationStateClosing::ID:
            std::cout << "Closing" << std::endl;
            break;

        case td_api::authorizationStateWaitTdlibParameters::ID:
        {
            auto request = td_api::make_object<td_api::setTdlibParameters>();

#if defined(TG_API_HASH) && defined(TG_API_ID)
            request->api_hash_ = TG_API_HASH;
            request->api_id_ = std::stoi(TG_API_ID);
#else
    #error TG_API_HASH and TG_API_ID must be defined!
#endif

            request->application_version_ = "1.0";
            request->database_directory_ = "db";
            request->device_model_ = "Desktop";
            request->files_directory_ = "downloads";
            request->system_language_code_ = "en";
            request->use_file_database_ = true;
            request->use_message_database_ = true;
            request->use_secret_chats_ = true;

            send(std::move(request));
            break;
        }

        case td_api::authorizationStateWaitPhoneNumber::ID:
        {
            std::cout << "Enter phone number: " << std::flush;
            std::string phone;
            std::cin >> phone;

            send(td_api::make_object<td_api::setAuthenticationPhoneNumber>(phone, nullptr));
            break;
        }

        case td_api::authorizationStateWaitEmailAddress::ID:
        {
            std::cout << "Enter email address: " << std::flush;
            std::string email;
            std::cin >> email;

            send(td_api::make_object<td_api::setAuthenticationEmailAddress>(email));
            break;
        };

        case td_api::authorizationStateWaitPassword::ID:
        {
            std::cout << "Enter authentication password: " << std::flush;
            std::string password;
            std::getline(std::cin, password);

            send(td_api::make_object<td_api::checkAuthenticationPassword>(password));
            break;
        }

        case td_api::authorizationStateWaitEmailCode::ID:
        {
            std::cout << "Enter email authentication code: " << std::flush;
            std::string code;
            std::cin >> code;

            send(
                td_api::make_object<td_api::checkAuthenticationEmailCode>(
                    td_api::make_object<td_api::emailAddressAuthenticationCode>(code)
                )
            );
            break;
        }

        case td_api::authorizationStateWaitCode::ID:
        {
            std::cout << "Enter authentication code: " << std::flush;
            std::string code;
            std::cin >> code;

            send(td_api::make_object<td_api::checkAuthenticationCode>(code));
            break;
        }

        case td_api::authorizationStateWaitOtherDeviceConfirmation::ID:
        {
            using Confirmation = td_api::authorizationStateWaitOtherDeviceConfirmation;

            auto auth_state = td::move_tl_object_as<Confirmation>(cur_state_->authorization_state_);
            std::cout << "Confirm this login link on another device: " << auth_state->link_ << std::endl;
        }
    }

    return false;
}

void
Authorization::send(TgRequestPtr request, ResponseHandler handler)
{
    if (handler)
    {
        client_.send(std::move(request), std::move(handler));
        return;
    }

    client_.send(
        std::move(request),
        [this, id = query_id_](TgObjectPtr object)
        {
            if (id == query_id_)
            {
                if (object->get_id() == td_api::error::ID)
                {
                    auto pError = td::move_tl_object_as<td_api::error>(object);
                    std::cout << "Got error during auth process: " << to_string(pError) << std::flush;

                    if (cur_state_)
                    {
                        handle(std::move(cur_state_));
                    }
                }
            }
        }
    );
}
