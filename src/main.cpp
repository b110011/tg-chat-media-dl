#include "client.hpp"
#include "auth.hpp"
#include "chat_bot_clicker.hpp"
#include "chat_media_downloader.hpp"

#include <fstream>
#include <iostream>
#include <optional>
#include <unordered_map>

td_api::int53
getSelfId(Client &client)
{
    td_api::int53 self_id = 0;

    client.send(
        td_api::make_object<td_api::getMe>(),
        [&self_id](td_api::object_ptr<td_api::user> user)
        {
            self_id = user->id_;
        }
    );
    client.pullUpdates(1);

    if (self_id == 0)
    {
        std::cerr << "Failed to retrieve id of current user!" << std::endl;
    }

    return self_id;
}

void
handleBotClicker(Client &client)
{
    std::cout << "Enter chat ID: " << std::flush;
    td_api::int53 chat_id;
    std::cin >> chat_id;

    std::cout << "Till message ID (0 - ignore): " << std::flush;
    td_api::int53 stopId;
    std::cin >> stopId;

    std::cout << "Enter button row: " << std::flush;
    td_api::int53 row_idx;
    std::cin >> row_idx;

    std::cout << "Enter button column: " << std::flush;
    td_api::int53 col_idx;
    std::cin >> col_idx;

    td_api::int53 self_id = getSelfId(client);
    if (self_id == 0)
    {
        return;
    }

    ChatBotClicker clicker(client, self_id, chat_id, stopId, row_idx, col_idx);
    clicker.run();
}

void
handleChatDownload(Client &client)
{
    std::cout << "Enter chat ID: " << std::flush;
    td_api::int53 chat_id;
    std::cin >> chat_id;

    if (chat_id < 0)
    {
        std::cerr << "We haven't supported downloading channels media yet!" << std::endl;
        return;
    }

    std::cout << "Participants ([p]articipant, [s]elf, [b]oth): " << std::flush;
    char participants;
    std::cin >> participants;

    std::cout << "Till message ID (0 - ignore): " << std::flush;
    td_api::int53 stopId;
    std::cin >> stopId;

    td_api::int53 self_id = getSelfId(client);
    if (self_id == 0)
    {
        return;
    }

    ChatMediaDownloader dl(client, self_id, chat_id, stopId, (ChatParticipants)participants);
    dl.run();
}

void
handleChatMessageInfo(Client &client)
{
    std::cout << "Enter chat ID: " << std::flush;
    td_api::int53 chat_id;
    std::cin >> chat_id;

    std::cout << "Enter message ID: " << std::flush;
    td_api::int53 message_id;
    std::cin >> message_id;

    client.send(
        td_api::make_object<td_api::getChatHistory>(
            chat_id,
            message_id,
            /* offset = */ -1,
            /* limit = */ 1,
            /* only_local = */ false
        ),
        [](TgObjectPtr object)
        {
            std::cout << td_api::to_string(object) << std::endl;
        }
    );
}

void
handleWipe(Client &client)
{
    std::cout << "Enter chat ID: " << std::flush;
    td_api::int53 chat_id;
    std::cin >> chat_id;

    std::cout << "Enter file with message ids: " << std::flush;
    std::string file_path;
    std::cin >> file_path;

    td_api::array<td_api::int53> msg_ids;
    auto deleteMessages = [&]()
    {
        client.send(
            td_api::make_object<td_api::deleteMessages>(
                chat_id,
                std::move(msg_ids),
                /* revoke = */ true
            )
        );
    };

    std::ifstream ifs{file_path};
    td_api::int53 msg_id = 0;

    while (ifs >> msg_id)
    {
        msg_ids.push_back(msg_id);
        std::cout << msg_id << std::endl;

        if (msg_ids.size() == 50)
        {
            deleteMessages();
        }
    }

    if (!msg_ids.empty())
    {
        deleteMessages();
    }

    // TODO: Remove deleted ids from file.
}

int
main()
{
    td::ClientManager::execute(td_api::make_object<td_api::setLogVerbosityLevel>(1));

    std::optional<Client> client{std::in_place};
    std::optional<Authorization> auth{std::in_place, *client};

    std::unordered_map<td_api::int53, std::string> chat_titles;

    char action = 'h';
    bool running = true;
    while (running)
    {
        if (auth)
        {
            auto response = client->receive(10);
            if (response.first && response.second)
            {
                if (auth->handle(std::move(response.first)))
                {
                    auth.reset();
                    client->pullUpdates(0);
                }
            }
            continue;
        }

        switch (action)
        {
            case 'q':  // quite
                return 0;

            case 't':  // close
                std::cout << "Closing..." << std::endl;
                client->send(
                    td_api::make_object<td_api::close>(),
                    [](TgObjectPtr /*object*/)
                    {
                        std::cout << "App is closed!" << std::endl;
                    }
                );
                running = false;
                break;

            case 'l':  // log out
                std::cout << "Logging out..." << std::endl;
                client->send(
                    td_api::make_object<td_api::logOut>(),
                    [](TgObjectPtr /*object*/)
                    {
                        std::cout << "You have been logged out!" << std::endl;
                    }
                );
                running = false;
                break;

            case 's':  // self
                client->send(
                    td_api::make_object<td_api::getMe>(),
                    [](TgObjectPtr object)
                    {
                        std::cout << td_api::to_string(object) << std::endl;
                    }
                );
                break;

            case 'c':  // chat list
                std::cout << "Loading chat list..." << std::endl;
                client->addHandler(
                    [&chat_titles](td_api::object_ptr<td_api::updateNewChat> update)
                    {
                        chat_titles[update->chat_->id_] = update->chat_->title_;
                    }
                );
                client->addHandler(
                    [&chat_titles](td_api::object_ptr<td_api::updateChatTitle> update)
                    {
                        chat_titles[update->chat_id_] = update->title_;
                    }
                );
                client->send(
                    td_api::make_object<td_api::getChats>(nullptr, 20),
                    [&chat_titles](td_api::object_ptr<td_api::chats> chats)
                    {
                        for (auto const chat_id : chats->chat_ids_)
                        {
                            std::cout << "[chat_id:" << chat_id << "] [title:" << chat_titles[chat_id] << "]" << std::endl;
                        }
                        std::cout << std::endl;
                    }
                );
                break;

            case 'm':  // message info
                handleChatMessageInfo(*client);
                break;

            case 'd':  // download
                handleChatDownload(*client);
                break;

            case 'w':  // wipe
                handleWipe(*client);
                break;

            case 'b':  // bot clicker
                handleBotClicker(*client);
                break;

            case 'h':
                std::cout << R"help(Actions
    [h] help,
    [q] quit,
    [t] close,
    [l] log out,
    [u] update,
    [s] show self,
    [c] show chats,
    [m] show message,
    [d] download chat media
    [w] wipe messages from chat
    [b] click bot messages
)help" << std::endl;
                break;

            case 'u':
            default:
                break;
        }

        client->pullUpdates(1);

        if (running)
        {
            std::cout << "Enter action: " << std::flush;
            std::cin >> action;
        }
    }

    std::cout << "All work is done!\nClosing gracefully :)" << std::endl;
    return 0;
}
