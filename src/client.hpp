#include <td/telegram/Client.h>

#include "types.hpp"
#include "utils.hpp"

#include <iostream>
// #include <fstream>
#include <memory>
#include <type_traits>
#include <unordered_map>

class Client final
{
public:

    Client();

    ~Client();


    void send(TgRequestPtr request, ResponseHandler handler = {});

    template<typename _ResponseHandlerT>
    void send(TgRequestPtr request, _ResponseHandlerT handler);

    // bool here indicates whether we received a valid response.
    // object might be empty even if bool is true. that means response was process by some handler.
    std::pair<TgObjectPtr, bool> receive(double timeout);


    void pullUpdates(double timeout);


    template<typename _ResponseHandlerT>
    void addHandler(_ResponseHandlerT handler);

    template<typename _ResponseT>
    void removeHandler();

private:

    void removeHandler(std::int32_t id);

private:

    using ClientId = td::ClientManager::ClientId;
    using RequestId = td::ClientManager::RequestId;

    std::unique_ptr<td::ClientManager> client_manager_;
    ClientId client_id;
    RequestId cur_query_id;

    std::unordered_map<RequestId, ResponseHandler> handlers_;
    std::unordered_map<std::int32_t, ResponseHandler> handlers_by_type_;

    // std::ofstream m_debug{"debug.log"};
};

template<typename _ResponseHandlerT>
void
Client::send(TgRequestPtr request, _ResponseHandlerT handler)
{
    using ResponsePtr = std::decay_t<first_argument<_ResponseHandlerT>>;

    if constexpr (std::is_same_v<ResponsePtr, TgObjectPtr>)
    {
        send(std::move(request), ResponseHandler{handler});
    }
    else
    {
        send(
            std::move(request),
            [h = std::move(handler)](TgObjectPtr object)
            {
                using ResponseType = typename ResponsePtr::element_type;

                if (object->get_id() == ResponseType::ID)
                {
                    h(td::move_tl_object_as<ResponseType>(object));
                    return;
                }

                std::cerr << "Wanted '" << object->get_id() << "' but got: " << to_string(object) << std::endl;
                throw std::runtime_error{"Unexpected response!"};
            }
        );
    }
}

template<typename _ResponseHandlerT>
void
Client::addHandler(_ResponseHandlerT handler)
{
    using ResponsePtr = std::decay_t<first_argument<_ResponseHandlerT>>;
    using ResponseType = typename ResponsePtr::element_type;
    static_assert(!std::is_same_v<ResponsePtr, TgObjectPtr>);

    handlers_by_type_.emplace(
        ResponseType::ID,
        [h = std::move(handler)](TgObjectPtr object)
        {
            h(td::move_tl_object_as<ResponseType>(object));
        }
    );
}

template<typename _ResponseT>
void
Client::removeHandler()
{
    static_assert(!std::is_base_of_v<_ResponseT, td_api::Object>);
    removeHandler(_ResponseT::ID);
}
