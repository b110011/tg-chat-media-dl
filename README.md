# Telegram Chat Media Downloader

This project does the similar job to the official [chat export tool](https://telegram.org/blog/export-and-more), except it ignores all forwarded messages.

**Table of contents**

- [Prerequisites](#prerequisites)
- [Building](#building)
  - [Local](#local)
    - [tdlib](#tdlib)
    - [downloader](#downloader)
  - [Docker](#docker)
- [Usage](#usage)
- [Disclaimer](#disclaimer)
- [License](#license)

## Prerequisites

Make sure you have installed all of the following prerequisites on your Ubuntu development machine:

```bash
sudo apt-get install --no-install-recommends -y \
    cmake \
    make \
    g++ \
    gcc \
    gperf \
    libssl-dev \
    zlib1g-dev
```

Next you need to download repo and apply [tdlib.patch](./tdlib.patch) to speed-up build time.

```bash
git clone https://gitlab.com/b110011/tg-chat-media-dl.git
git submodule update --init
cd tdlib
git apply ../tdlib.patch
```

## Building

### Local

#### tdlib

First we need to build and install [tdlib](https://github.com/tdlib/td).

```bash
cmake -Stdlib -Bbuild_tdlib -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=../td
cmake --build build_tdlib --target install -j8
```

#### downloader

Now we can build downloader.

```bash
cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release
cmake --build build -j8
```

By default downloader will be built with `api_id` and `api_hash` taken from an [official c++ example](https://github.com/tdlib/td/blob/master/example/cpp/td_example.cpp).

If you want to use your own `api_id` and `api_hash`, you can specify them like this:

```bash
cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release -DTG_API_ID=<your_api_id> -DTG_API_HASH=<your_api_hash>
cmake --build build -j8
```

> Please remember that once you've create an app, you can't revoke it.
> ([thread 1](https://stackoverflow.com/questions/59663483/how-to-delete-telegram-app-api-key-api-hash/66961876#66961876),
> [thread 2](https://stackoverflow.com/questions/52259561/how-to-delete-destroy-my-telegram-api-id-and-api-hash#answer-66263448))

### Docker

There is also an option to build and use this project in form of a docker image.

```bash
docker build -t tg .
# or
docker build -t tg --build-arg TG_API_ID=<your_api_id> --build-arg TG_API_HASH=<your_api_hash> .
```

## Usage

You can use this app directly (`./build/tg-chat-media-dl`) or run it in docker:

```bash
mkdir {db,downloads}
docker run -it -v ./db:/data/db -v ./downloads:/data/downloads -u ${UID} tg
```

and follow instructions.

---

If you want to capture output for further investigation, run downloader like this:

```bash
./build/tg-chat-media-dl | tee downloads/output.log
```

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## License

This project is released under the [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.txt) license.
