FROM debian:bullseye-slim  as build-stage

ARG TG_API_ID
ARG TG_API_HASH

WORKDIR /tmp

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        cmake \
        g++ \
        gcc \
        gperf \
        libssl-dev \
        make \
        zlib1g-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY . .

RUN cmake -Stdlib -Bbuild_tdlib -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=../td && \
    cmake --build build_tdlib --target install -j8

RUN cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && \
    cmake --build build -j8

FROM debian:bullseye-slim

COPY --from=build-stage /tmp/build/tg-chat-media-dl /usr/bin

WORKDIR /data/

CMD ["/usr/bin/tg-chat-media-dl"]
