#!/usr/bin/env python3

import argparse
import shutil
from pathlib import Path
from typing import cast

#
# args
#

parser = argparse.ArgumentParser(
    description="A helper script to remove files from a given folder.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-o",
    "--output-dir",
    type=Path,
    help="A directory with downloaded and sorted files",
    default="output"
)
parser.add_argument(
    "-c",
    "--cache",
    type=Path,
    help="A path to cache which contains message id and file name pairs",
    default="output/rename.csv"
)
parser.add_argument(
    "-b",
    "--backup-dir",
    type=Path,
    help="A path where to move found files",
    default="backup"
)
args = parser.parse_args()

#
# logic
#

files: set[str] = set()
with cast(Path, args.cache).open() as file:
    file.readline()  # skip header
    for line in file:
        files.add(line.rstrip().split(",")[1])

args.backup_dir.mkdir(exist_ok=True, parents=True)

for file_path in cast(Path, args.output_dir).rglob("*"):
    if file_path.is_file() and (file_path.name in files):
        new_file_path = cast(Path, args.backup_dir) / file_path.name

        print(f"{file_path} -> {new_file_path}")
        shutil.move(file_path, new_file_path)
