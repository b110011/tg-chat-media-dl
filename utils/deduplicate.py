#!/usr/bin/env python3

import argparse

#
# args
#

parser = argparse.ArgumentParser(
    description="A helper script to deduplicate messages in downloader's cache.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-f",
    "--file",
    help="A downloader cache file",
    default="downloads/cache.csv"
)
args = parser.parse_args()

#
# logic
#

files_info: dict[str, list[str]] = {}
header: str = ""

with open(args.file) as file:
    header = file.readline()
    for line in file:
        file_info = line.split(",")
        msg_id = file_info[0]

        files_info.setdefault(msg_id, file_info)

with open(args.file, "w") as file:
    file.write(header)
    for file_info in files_info.values():
        file.write(",".join(file_info))
