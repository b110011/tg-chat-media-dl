#!/usr/bin/env python3

import argparse
import shutil
from dataclasses import dataclass, field
from datetime import datetime
from pathlib import Path

#
# args
#

parser = argparse.ArgumentParser(
    description="A helper script for renaming downloaded files.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-i",
    "--input-dir",
    help="A directory with downloaded files",
    default="downloads"
)
parser.add_argument(
    "-o",
    "--output-format",
    help="Output format",
    default="output/{sender_id}/{date:%Y}/{date:%m-%B}/Unknown/{date:%Y-%m-%d_%H-%M-%S}{album_idx}.{ext}"
)
parser.add_argument(
    "-c",
    "--cache",
    type=Path,
    help="A path to cache which contains message id and file name pairs",
    default="output/rename.csv"
)
args = parser.parse_args()

#
# logic
#


@dataclass(eq=False, kw_only=True, slots=True)
class FileInfo:
    msg_id: int
    sender_id: int
    file_id: int
    date: datetime
    file_path: Path
    in_album: bool
    skip: bool = field(default=True)


files_info: list[FileInfo] = []

with open(f"{args.input_dir}/cache.csv") as file:
    file.readline()  # skip header
    for line in file:
        file_info = line.rstrip().split(",")
        files_info.append(
            FileInfo(
                msg_id=int(file_info[0]),
                sender_id=int(file_info[1]),
                file_id=int(file_info[2]),
                date=datetime.utcfromtimestamp(int(file_info[3])),
                file_path=Path(file_info[4]),
                in_album=(file_info[5] == "1"),
            )
        )

files_info.sort(key=lambda fi: fi.msg_id)  # from oldest message to the newest one

# We don't detect duplicated files when processing chat messages because we iterate
# from the newest one to the oldest one.
unique_files: dict[Path, int] = {}
for file_info in files_info:
    old_msg_id = unique_files.get(file_info.file_path)
    if old_msg_id:
        print(f"{file_info.msg_id} won't be downloaded as it duplicates {old_msg_id}!")
        continue

    file_info.skip = False
    unique_files[file_info.file_path] = file_info.msg_id

args.cache.parent.mkdir(exist_ok=True, parents=True)
with open(args.cache, "w") as rename_log:
    rename_log.write("MSG_ID,FILE_NAME\n")

    album_idx = 0
    cache: dict[int, Path] = {}
    for file_info in files_info:
        if file_info.skip:
            rename_log.write(f"{file_info.msg_id},{cache[file_info.file_id]}\n")
            continue

        file_name = file_info.file_path.stem
        file_ext = file_info.file_path.suffix.removeprefix(".")

        if file_info.in_album:
            album_idx += 1
            album_idx_str = f"+{album_idx}"
        else:
            album_idx = 0
            album_idx_str = ""

        new_file = Path(
            args.output_format.format(
                album_idx=album_idx_str,
                date=file_info.date,
                ext=file_ext,
                sender_id=file_info.sender_id,
            )
        )
        new_file.parent.mkdir(exist_ok=True, parents=True)

        print(f"{file_info.file_path} -> {new_file}")
        shutil.copyfile(file_info.file_path, new_file)

        rename_log.write(f"{file_info.msg_id},{new_file.name}\n")
        cache[file_info.file_id] = new_file.name
