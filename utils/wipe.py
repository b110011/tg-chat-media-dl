#!/usr/bin/env python3

import argparse
from pathlib import Path
from typing import cast

#
# args
#

parser = argparse.ArgumentParser(
    description="A helper script to create a wipe list from a given folder.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "-o",
    "--output-dir",
    type=Path,
    help="A directory with downloaded and sorted files",
    default="output"
)
parser.add_argument(
    "-c",
    "--cache",
    type=Path,
    help="A path to cache which contains message id and file name pairs",
    default="output/rename.csv"
)
parser.add_argument(
    "-r",
    "--result",
    type=Path,
    help="A path to a resulting file with message ids",
    default="output/wipe.txt"
)
args = parser.parse_args()

#
# logic
#

file2msgid: dict[str, int] = {}
with cast(Path, args.cache).open() as file:
    file.readline()  # skip header
    for line in file:
        msg_id, file_name = line.rstrip().split(",")
        file2msgid[file_name] = int(msg_id)

with cast(Path, args.result).open("w") as wipe_log:
    for file_path in cast(Path, args.output_dir).rglob("*"):
        if file_path.is_file():
            msg_id = file2msgid.get(file_path.name)
            if msg_id:
                wipe_log.write(f"{msg_id}\n")
