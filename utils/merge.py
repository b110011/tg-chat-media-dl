#!/usr/bin/env python3

import argparse

#
# args
#

parser = argparse.ArgumentParser(
    description="A helper script to merge caches. Useful when something when wrong and file paths disappeared.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-o", "--output", help="Output file", default="merged.txt")
parser.add_argument("-i", "--input", nargs="+", help="Input files", required=True)
args = parser.parse_args()

#
# logic
#

files_info: dict[int, list[str]] = {}
header: str = ""

for file_path in args.input:
    with open(file_path) as file:
        header = file.readline()
        for line in file:
            file_info = line.split(",")
            msg_id = file_info[0]

            files_info.setdefault(msg_id, file_info[1:])

merged_files_info: list[list[str]] = []
for msg_id, other_info in files_info.items():
    merged_files_info.append([msg_id, *other_info])

merged_files_info.sort(key=lambda x: x[0])

# no need to add new lines, because 'readline' doesn't remove it.
with open(args.output, "w") as file:
    file.write(header)
    for file_info in merged_files_info:
        file.write(",".join(file_info))
